#!/usr/bin/env python3
# coding: utf-8

import socket
import sys

host = 'localhost'
dict_port = 2628

try:
    s = socket.create_connection((host, dict_port))
except socket.gaierror:
    print('Hostname %s could not be resolved!' % host)
    sys.exit()

s.sendall('DEFINE * březen'.encode('utf8'))
# s.sendall('HELP"\n')

recv_msg = s.recv(4096)
print(recv_msg)

s.close()
