#!/usr/bin/env python3
# -*- coding: utf8 -*-

#  Dict protocol client for Gedit
#
#  Copyright (C) 2015 Matěj Cepl
#
#  This program is free software: you can redistribute it and/or modify it
#  under the terms of the GNU General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful, but WITHOUT
#  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
#  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
#  for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program.  If not, see <http://www.gnu.org/licenses/>.

import gettext
import logging
import re
#import sys
from socket import socket
from urllib.request import URLError, urlopen
from urllib.parse import urlencode

import html2text

from bs4 import BeautifulSoup
from gi.repository import GObject, Gedit, Gio, Gtk


ACCELERATOR = ['<Ctrl><Alt>d']

class DictPluginAppActivatable(GObject.Object, Gedit.AppActivatable):
    __gtype_name__ = "DictPlugin"
    app = GObject.Property(type=Gedit.App)

    def __init__(self):
        GObject.Object.__init__(self)

    def do_activate(self):
        self.app.set_accels_for_action("win.dict", ACCELERATOR)

        # Dict actions below, hardcoding domain here to avoid
        # complications now
        _ = lambda s: gettext.dgettext('devhelp', s)

        self.menu_ext = self.extend_menu("tools-section")
        item = Gio.MenuItem.new(_("Dict"), "win.dict")
        self.menu_ext.prepend_menu_item(item)

    def do_deactivate(self):
        self.app.set_accels_for_action("win.dict", [])
        self.menu_ext = None

    def do_update_state(self):
        pass


class DictPluginWindowActivatable(GObject.Object,
                                  Gedit.WindowActivatable):
    window = GObject.property(type=Gedit.Window)

    def __init__(self):
        GObject.Object.__init__(self)

    def do_activate(self):
        action = Gio.SimpleAction(name="dict")
        action.connect('activate', self.on_dict_text_activate)
        self.window.add_action(action)

    def on_dict_text_activate(self, action, parameter, user_data=None):
        orig_word = self._prompt_for_original('Enter the word to dict:')
        if orig_word:
            dictd = self._dict(orig_word)
            self._alert(dictd)

    # Based on https://ardoris.wordpress.com/2008/07/05\
    #    /pygtk-text-entry-dialog/
    def _prompt_for_original(self, prompt_text):
        def response_to_dialog(entry, dialog, response):
            dialog.response(response)

        dlg = Gtk.MessageDialog(self.window,
                                Gtk.DialogFlags.MODAL |
                                Gtk.DialogFlags.DESTROY_WITH_PARENT,
                                Gtk.MessageType.QUESTION,
                                Gtk.ButtonsType.OK,
                                None)
        dlg.set_markup(prompt_text)

        entry = Gtk.Entry()
        entry.connect('activate', response_to_dialog,
                      dlg, Gtk.ResponseType.OK)
        dlg.vbox.pack_end(entry, False, False, 5)
        dlg.show_all()

        dlg.run()
        text = entry.get_text().strip()
        dlg.destroy()
        return text

    # Based on https://ardoris.wordpress.com/2008/07/05\
    #    /pygtk-text-entry-dialog/
    def _alert(self, out_text):
        dlg = Gtk.MessageDialog(self.window,
                                Gtk.DialogFlags.MODAL |
                                Gtk.DialogFlags.DESTROY_WITH_PARENT,
                                Gtk.MessageType.INFO,
                                Gtk.ButtonsType.OK,
                                None)
        dlg.set_markup(out_text)
        dlg.show_all()
        dlg.run()
        dlg.destroy()

    def _dict(self, word):
        base_url = "http://slovnik.seznam.cz/"
        from_language = "cz"
        to_language = "en"
        out = ''

        if from_language == to_language:
            return word
        else:
            query = urlencode({'q': word})
            url = base_url + "/%s-%s/word/?" \
                % (from_language, to_language) + query
            try:
                inf = urlopen(url)
            except URLError:
                logging.error('url {} cannot be opened'.format(url))
            else:
                result = inf.read()
                inf.close()
                bs = BeautifulSoup(result, 'html.parser')
                our_div = bs.find(id='fastTrans')
                if our_div is not None:
                    for br in our_div.find_all('br'):
                        br.replace_with('$#$#$#')
                    out = html2text.HTML2Text().handle(our_div.get_text())
                    out = out.replace('\n', ' ').replace(' , ', ', ')
                    out = re.sub('\s*\$#\$#\$#\s*', '\n', out).strip()
                return out

# ex:ts=4:et:
