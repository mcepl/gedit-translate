Translate gedit plugin
===================

This plugin allows translate a word from Czech to English
(configuration of the language is on the to-do list, but I am not
in hurry; patches welcome!).


Install
-------

Copy the files translate.plugin and translate.py into your gedit
plugin directory (most likely on Linux it is
~/.local/share/gedit/plugins/), then restart gedit.


Usage
-----

After installation a new command 'Translate' is available in the
edit menu.  You can also directly use <alt>-p.


Contact
-------

Matěj Cepl <mcepl@cepl.eu>

